
# Programs and parameters
CC      = gcc
CFLAGS  = -Wall -std=gnu99 `pkg-config --cflags gtk+-3.0`

LD      = gcc
LDFLAGS = `pkg-config --libs gtk+-3.0`

ifdef DEBUG
	CFLAGS  += -g -O0
	LDFLAGS +=
else
	CFLAGS  += -Ofast -DNDEBUG
	LDFLAGS += -s
endif

RM      = rm
RMFLAGS = -f

SRCS := $(wildcard *.c)
OBJS := $(patsubst %.c, %.o, $(SRCS))

TARGET = gitodouble-gtk


.PHONY: clean run


$(TARGET): $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	$(RM) $(RMFLAGS) $(TARGET) $(OBJS) *.o

run: $(TARGET)
	./$(TARGET) todo-list.txt &
