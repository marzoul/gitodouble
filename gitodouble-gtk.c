/*
 * This is gitodouble-gtk, a simple and lightweight todo-list editor,
 * friendly for version control systems.
 * Copyright (C) 2016 by Adrien Prost-Boucle <adrien.prost-boucle@laposte.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// For declaration of function strcasestr()
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Declarations for regular expressions
#include <sys/types.h>
#include <regex.h>

#include <gtk/gtk.h>

#define GITODOUBLE_NAME     "gitodouble-gtk"
#define GITODOUBLE_VERSION  "0.1"
#define GITODOUBLE_AUTHOR   "Adrien Prost-Boucle <adrien.prost-boucle@laposte.net>"

#define FILE_VERSION    "gitodouble v1"
#define DEFAULT_MARKER  "$#"

#define SPACECHARS      " \t\r\n"
#define SPACECOMMA      " \t\r\n,"

#define CSS_STATUS_RED  "statusred"
#define CSS_BG_GREY     "graybg"

#define ACCMAP_OPEN       "<gitodouble>/open"
#define ACCMAP_SAVE       "<gitodouble>/save"
#define ACCMAP_QUIT       "<gitodouble>/quit"
#define ACCMAP_NEWTAG     "<gitodouble>/newtag"
#define ACCMAP_NEWTAG_CNT "<gitodouble>/newtagcnt"
#define ACCMAP_NEWITEM    "<gitodouble>/newitem"
#define ACCMAP_REFRESH    "<gitodouble>/refresh"
#define ACCMAP_SEARCH     "<gitodouble>/search"

#define SPACING_PIX  5



//================================================
// Constants and structures
//================================================

//#define dbgprintf(fmt, ...) printf("DEBUG %s:%u - " fmt, __FILE__, __LINE__, ##__VA_ARGS__)
#define dbgprintf(fmt, ...) do { } while(0)

// Tag status
#define TAG_STATUS_NONE  0
#define TAG_STATUS_WITH  1
#define TAG_STATUS_WOUT  2

// Columns for area of tags
#define TAGS_COL_NAME  0
#define TAGS_COL_COUNT 1
#define TAGS_COL_WITH  2
#define TAGS_COL_WOUT  3
#define TAGS_COL_IDX   4

// Columns for area of filtered items
#define FILT_COL_ID     0
#define FILT_COL_TITLE  1
#define FILT_COL_IDX    2

// Values for sorting
#define SORT_NONE   0
#define SORT_DEC    1
#define SORT_INC    2

typedef struct tag_t  tag_t;
struct tag_t {
	gchar*   name;
	unsigned index;
	// Whether the item has counter
	gboolean has_counter;
	unsigned counter;
	// Sorting options
	unsigned status;
	// Internal counter of items, to remove the tag when unused
	unsigned items_nb;
};

typedef struct item_t  item_t;
struct item_t {
	gchar*   id;
	unsigned index;

	unsigned tags_nb;
	unsigned tags_arrsize;
	tag_t**  tags_arr;

	// This is the first line of the content (without newline char)
	char*    title;

	gchar*   marker;
	char*    content;

	gboolean search_visible;
	gboolean tags_visible;
};

typedef struct control_t  control_t;
struct control_t {

	// Miscellaneous status variables

	gchar*   filename;
	gboolean file_modified;

	// The file contents

	gchar* marker;

	char* desc_marker;
	char* desc_content;

	unsigned tags_nb;
	unsigned tags_arrsize;
	tag_t*   tags_arr;

	unsigned items_nb;
	unsigned items_arrsize;
	item_t*  items_arr;

	item_t*  item_selected;

	// The GUI

	GtkWidget* window;
	GtkWidget* grid;

	GtkWidget* hpaned;  // tags on left, items & edit on right
	GtkWidget* vpaned;  // items on top, edit on bottom

	GtkCellRenderer* cellrender_text;

	GtkWidget*         tags_scroll;
	GtkListStore*      tags_list_store;
	GtkWidget*         tags_tree_view;
	GtkTreeViewColumn* tags_viewcol_name;
	GtkTreeViewColumn* tags_viewcol_count;
	GtkTreeViewColumn* tags_viewcol_with;
	GtkTreeViewColumn* tags_viewcol_wout;
	GtkCellRenderer*   tags_render_name;
	GtkCellRenderer*   tags_render_count;
	GtkCellRenderer*   tags_render_with;
	GtkCellRenderer*   tags_render_wout;
	GtkTreeSelection*  tags_selection;
	unsigned           tags_sorting;

	GtkWidget*         filt_scroll;
	GtkListStore*      filt_list_store;
	GtkWidget*         filt_tree_view;
	GtkCellRenderer*   filt_cell_render;
	GtkTreeViewColumn* filt_viewcol_id;
	GtkTreeViewColumn* filt_viewcol_title;
	GtkTreeSelection*  filt_selection;
	unsigned           filt_sorting;

	GtkWidget*         text_grid;
	GtkWidget*         text_entry_id;
	GtkWidget*         text_entry_tags;
	GtkWidget*         text_hpaned_entries;  // id in left, flags on right
	GtkWidget*         text_scroll;
	GtkWidget*         text_edit;
	GtkTextBuffer*     text_buffer;
	GtkTextTag*        text_tag_link;

	regex_t            search_regex;
	GtkWidget*         search_grid;
	GtkWidget*         search_entry;

	GtkWidget*         ckeckbox_search_regexp;

	GtkWidget*         statusbar;
	GtkCssProvider*    css_provider;
};



//================================================
// Miscellaneous
//================================================

#define ISDIGIT(v) ((v) >= '0' && (v) <= '9')

// Comparison function for strings: make sure "bug1" < "bug10" and "bug2" < "bug10"
static gint gstrcmp_fixnumbers(const gchar* gstr1, const gchar* gstr2) {
	gchar c1, c2;
	do {
		c1 = *gstr1;
		c2 = *gstr2;
		if(c1==0 || c2==0) break;
		if(ISDIGIT(c1) && ISDIGIT(c2)) {
			gchar* end1 = NULL;
			gchar* end2 = NULL;
			guint64 u1 = g_ascii_strtoull(gstr1, &end1, 10);
			guint64 u2 = g_ascii_strtoull(gstr2, &end2, 10);
			if(u1 != u2) return u1 < u2 ? -1 : 1;
			gstr1 = end1;
			gstr2 = end2;
		}
		else {
			if(c1 != c2) return c1 - c2;
			gstr1++;
			gstr2++;
		}
	} while(1);
	return c1 - c2;
}

static gint treecmp_gstrcmp_fixnumbers(GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer user_data) {
	// Get the strings
	const gchar* gstr1 = NULL;
	const gchar* gstr2 = NULL;
	gtk_tree_model_get(model, a, FILT_COL_ID, &gstr1, -1);
	gtk_tree_model_get(model, b, FILT_COL_ID, &gstr2, -1);
	// A bit of safety
	if(gstr1==NULL || gstr2==NULL) return 0;
	// Do actual comparison
	return gstrcmp_fixnumbers(gstr1, gstr2);
}



//================================================
// Window title
//================================================

static void window_title_update(control_t *CT) {
	if(CT->filename != NULL) {
		unsigned len = strlen(CT->filename);
		char buf[len + 128];
		sprintf(buf, GITODOUBLE_NAME " - %s", CT->filename);
		gtk_window_set_title(GTK_WINDOW(CT->window), buf);
	}
	else {
		gtk_window_set_title(GTK_WINDOW(CT->window), GITODOUBLE_NAME);
	}
}



//================================================
// Status bar
//================================================

static void statusbar_set_internal(control_t *CT, const char* msg) {
	gtk_statusbar_remove_all(GTK_STATUSBAR(CT->statusbar), 0);
	gtk_statusbar_push(GTK_STATUSBAR(CT->statusbar), 0, msg);
}

static void statusbar_set(control_t *CT, const char* msg) {
	// Ensure we use the default color
	GtkStyleContext* context = gtk_widget_get_style_context(CT->statusbar);
	gtk_style_context_remove_class(context, CSS_STATUS_RED);
	// Write the message
	statusbar_set_internal(CT, msg);
}

static void statusbar_set_error(control_t *CT, const char* msg) {
	// Change the color
	GtkStyleContext* context = gtk_widget_get_style_context(CT->statusbar);
	gtk_style_context_add_class(context, CSS_STATUS_RED);
	// Write the message
	statusbar_set_internal(CT, msg);
}



//================================================
// Activate / disable edit area
//================================================

static void edit_area_enable(control_t *CT) {
	// Ensure we use the default text style
	GtkStyleContext *context = NULL;
	context = gtk_widget_get_style_context(CT->text_grid);
	gtk_style_context_remove_class(context, CSS_BG_GREY);

	// Make the edit area editable
	GValue gval = G_VALUE_INIT;
	g_value_init(&gval, G_TYPE_BOOLEAN);
	g_value_set_boolean(&gval, TRUE);
	g_object_set_property(G_OBJECT(CT->text_entry_id), "editable", &gval);
	g_object_set_property(G_OBJECT(CT->text_entry_tags), "editable", &gval);
	g_value_unset(&gval);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(CT->text_edit), TRUE);

	// Clear any previous content
	gtk_entry_set_text(GTK_ENTRY(CT->text_entry_id), "");
	gtk_entry_set_text(GTK_ENTRY(CT->text_entry_tags), "");
	gtk_text_buffer_set_text(CT->text_buffer, "", -1);
}

static void edit_area_disable(control_t *CT) {
	// Change the text style
	GtkStyleContext *context = NULL;
	context = gtk_widget_get_style_context(CT->text_grid);
	gtk_style_context_add_class(context, CSS_BG_GREY);

	// Make the edit area editable
	GValue gval = G_VALUE_INIT;
	g_value_init(&gval, G_TYPE_BOOLEAN);
	g_value_set_boolean(&gval, FALSE);
	g_object_set_property(G_OBJECT(CT->text_entry_id), "editable", &gval);
	g_object_set_property(G_OBJECT(CT->text_entry_tags), "editable", &gval);
	g_value_unset(&gval);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(CT->text_edit), FALSE);

	// Set phony content for information
	gtk_entry_set_text(GTK_ENTRY(CT->text_entry_id), "ID");
	gtk_entry_set_text(GTK_ENTRY(CT->text_entry_tags), "Tags");
	gtk_text_buffer_set_text(CT->text_buffer, "Item content", -1);
}



//================================================
// Manipulation of tags and items
//================================================

static void tag_set_status_fromiter(control_t *CT, GtkTreeIter *iter, unsigned status) {
	// Get the tag index
	guint idx;
	gtk_tree_model_get(GTK_TREE_MODEL(CT->tags_list_store), iter, TAGS_COL_IDX, &idx, -1);

	tag_t* tag = CT->tags_arr + idx;
	tag->status = status;

	GValue a = G_VALUE_INIT;

	// Set the filter "with"
	g_value_init(&a, G_TYPE_BOOLEAN);
	g_value_set_boolean(&a, FALSE);
	if(status==TAG_STATUS_WITH) g_value_set_boolean(&a, TRUE);
	gtk_list_store_set_value(CT->tags_list_store, iter, TAGS_COL_WITH, &a);
	g_value_unset(&a);

	// Set the filter "without"
	g_value_init(&a, G_TYPE_BOOLEAN);
	g_value_set_boolean(&a, FALSE);
	if(status==TAG_STATUS_WOUT) g_value_set_boolean(&a, TRUE);
	gtk_list_store_set_value(CT->tags_list_store, iter, TAGS_COL_WOUT, &a);
	g_value_unset(&a);
}

static void tags_view_append(control_t *CT, tag_t* tag, GtkTreeIter *iter) {
	GtkTreeIter iter_i;
	if(iter==NULL) iter = &iter_i;

	// Acquire an iterator
	gtk_list_store_append(CT->tags_list_store, iter);
	GValue a = G_VALUE_INIT;

	// Set the tag index
	g_value_init(&a, G_TYPE_UINT);
	g_value_set_uint(&a, tag->index);
	gtk_list_store_set_value(CT->tags_list_store, iter, TAGS_COL_IDX, &a);
	g_value_unset(&a);

	// Set the tag name
	g_value_init(&a, G_TYPE_STRING);
	g_value_set_string(&a, tag->name);
	gtk_list_store_set_value(CT->tags_list_store, iter, TAGS_COL_NAME, &a);
	g_value_unset(&a);

	// Set the tag counter
	if(tag->has_counter==TRUE) {
		g_value_init(&a, G_TYPE_UINT);
		g_value_set_uint(&a, tag->counter);
		gtk_list_store_set_value(CT->tags_list_store, iter, TAGS_COL_COUNT, &a);
		g_value_unset(&a);
	}

	// Set the status
	tag_set_status_fromiter(CT, iter, tag->status);
}

static tag_t* tag_new(control_t *CT, GtkTreeIter *iter, const gchar* tagname, unsigned status, gboolean with_counter) {

	// Ensure there is enough space in array
	if(CT->tags_nb >= CT->tags_arrsize) {
		unsigned newsize = CT->tags_arrsize + CT->tags_arrsize / 2 + 128;
		CT->tags_arr = realloc(CT->tags_arr, newsize * sizeof(*CT->tags_arr));
		memset(CT->tags_arr + CT->tags_arrsize, 0, (newsize - CT->tags_arrsize) * sizeof(*CT->tags_arr));
		CT->tags_arrsize = newsize;
	}

	unsigned idx = CT->tags_nb++;
	tag_t* tag = CT->tags_arr + idx;
	tag->index = idx;

	// Clear any previous content
	if(tag->name != NULL) { g_free(tag->name); tag->name = NULL; }
	tag->has_counter = FALSE;
	tag->counter = 0;
	tag->has_counter = with_counter;
	tag->status = TAG_STATUS_NONE;

	// Set new content
	tag->name = g_strdup(tagname);

	// Append to the tags view
	tags_view_append(CT, tag, iter);

	// Mark the file as modified
	CT->file_modified = TRUE;

	return tag;
}

static tag_t* tag_get(control_t *CT, const gchar* tagname) {
	for(unsigned k=0; k<CT->tags_nb; k++) {
		tag_t* tag = CT->tags_arr + k;
		if(tag->name==NULL) continue;
		if(strcmp(tag->name, tagname)==0) return tag;
	}
	return NULL;
}

static item_t* item_new(control_t *CT) {

	// Ensure there is enough space in array
	if(CT->items_nb >= CT->items_arrsize) {
		unsigned newsize = CT->items_arrsize + CT->items_arrsize / 2 + 128;
		CT->items_arr = realloc(CT->items_arr, newsize * sizeof(*CT->items_arr));
		memset(CT->items_arr + CT->items_arrsize, 0, (newsize - CT->items_arrsize) * sizeof(*CT->items_arr));
		CT->items_arrsize = newsize;
	}

	item_t* item = CT->items_arr + CT->items_nb;
	item->index = CT->items_nb++;

	// Clear any previous content
	item->tags_nb = 0;
	if(item->id != NULL) { g_free(item->id); item->id = NULL; }
	if(item->title != NULL) { g_free(item->title); item->title = NULL; }
	if(item->marker != NULL) { g_free(item->marker); item->marker = NULL; }
	if(item->content != NULL) { g_free(item->content); item->content = NULL; }
	item->search_visible = TRUE;
	item->tags_visible = TRUE;

	// Set new content
	item->marker = g_strdup(DEFAULT_MARKER);

	// Mark the file as modified
	CT->file_modified = TRUE;

	return item;
}

static tag_t* item_tag_add(control_t *CT, item_t* item, const gchar* tagname) {

	// Ensure there is enough space in item array
	if(item->tags_nb >= item->tags_arrsize) {
		unsigned newsize = item->tags_arrsize + item->tags_arrsize / 2 + 16;
		item->tags_arr = realloc(item->tags_arr, newsize * sizeof(*item->tags_arr));
		memset(item->tags_arr + item->tags_arrsize, 0, (newsize - item->tags_arrsize) * sizeof(*item->tags_arr));
		item->tags_arrsize = newsize;
	}

	tag_t* tag = tag_get(CT, tagname);
	if(tag==NULL) tag = tag_new(CT, NULL, tagname, TAG_STATUS_NONE, FALSE);
	item->tags_arr[item->tags_nb++] = tag;

	// Note: don't marke the file as modifid here.
	// Because this function is called when updating tags, which may not have changed.

	return tag;
}

// Return the index in array of tags, or -1 if not found
static int item_tag_getidx(control_t *CT, item_t* item, tag_t* tag) {
	for(unsigned i=0; i<item->tags_nb; i++) {
		if(item->tags_arr[i]==tag) return i;
	}
	return -1;
}
// Return the number of items deleted (0 or 1)
static unsigned item_tag_remove(control_t *CT, item_t* item, tag_t* tag) {
	int idx = item_tag_getidx(CT, item, tag);
	if(idx < 0) return 0;

	// Shift the content of the array
	if(idx < item->tags_nb-1) memmove(item->tags_arr + idx, item->tags_arr + idx + 1, (item->tags_nb - idx - 1) * sizeof(*item->tags_arr));
	// Decrement the number of tags
	item->tags_nb --;

	return 1;
}

static gboolean marker_possible(const char* content, const char* marker) {
	if(content==NULL) return TRUE;
	unsigned markerlen = strlen(marker);
	const char* ptr_beg = content;
	while(*ptr_beg!=0) {
		char* ptr_endl = strchr(ptr_beg, '\n');
		if(ptr_endl==NULL) {
			if(strcmp(ptr_beg, marker)==0) return FALSE;
			break;
		}
		else {
			unsigned len = ptr_endl - ptr_beg;
			if(len >= markerlen) {
				if(strncmp(ptr_beg, marker, len)==0) return FALSE;
			}
		}
		if(*ptr_endl==0) break;
		ptr_beg = ptr_endl + 1;
	}
	return TRUE;
}

// Warning: the string is modified
static void item_tags_parse(control_t *CT, item_t* item, gchar* tags) {
	// Remove the previous tags
	item->tags_nb = 0;
	// Split the string
	char* ptr = tags;
	while(ptr!=NULL) {
		ptr += strspn(ptr, SPACECOMMA);  // Skip leading spaces
		char* ptr_beg = strsep(&ptr, SPACECOMMA);
		if(ptr_beg!=NULL && *ptr_beg!=0) item_tag_add(CT, item, ptr_beg);
	}
}

static void item_title_from_content(control_t *CT, item_t* item) {
	if(item->title != NULL) { g_free(item->title); item->title=NULL; }
	if(item->content == NULL) return;

	// Skip spaces at beginning
	gchar* ptr_beg = item->content + strspn(item->content, SPACECHARS);
	if(*ptr_beg == 0) return;

	// Get a proper end of line
	gchar* ptr_end = ptr_beg;  // Track the last non-space character
	for(gchar* ptr = ptr_beg+1; (*ptr)!=0 && (*ptr)!='\n'; ptr++) if(g_ascii_isspace(*ptr)==0) ptr_end = ptr;

	// Set the title inside the item
	unsigned len = ptr_end - ptr_beg + 1;
	item->title = g_strndup(ptr_beg, len);
}

static void items_filter_tags(control_t *CT) {

	// Count tags that must be present
	unsigned tags_with_nb = 0;
	for(unsigned t=0; t<CT->tags_nb; t++) {
		if(CT->tags_arr[t].status == TAG_STATUS_WITH) tags_with_nb ++;
	}

	// Scan all items
	for(unsigned i=0; i<CT->items_nb; i++) {
		item_t* item = CT->items_arr + i;
		item->tags_visible = FALSE;
		if(item->id==NULL) continue;
		// Scan the item tags
		unsigned with_nb = 0;
		unsigned wout_nb = 0;
		for(unsigned t=0; t<item->tags_nb; t++) {
			unsigned status = item->tags_arr[t]->status;
			if(status==TAG_STATUS_WITH) with_nb++;
			else if(status==TAG_STATUS_WOUT) { wout_nb++; break; }
		}
		// Decide whether the item can be displayed
		if(tags_with_nb==0 || with_nb >= tags_with_nb) item->tags_visible = TRUE;
		if(wout_nb > 0) item->tags_visible = FALSE;
	}

}

static void items_filter_search(control_t *CT, const gchar* strsearch) {

	if(strsearch==NULL || strsearch[0]==0) {
		for(unsigned i=0; i<CT->items_nb; i++) {
			item_t* item = CT->items_arr + i;
			if(item->id==NULL) item->search_visible = FALSE;
			else item->search_visible = TRUE;
		}
		return;
	}

	gboolean regex_mode = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(CT->ckeckbox_search_regexp));
	if(regex_mode==TRUE) {
		int z = regcomp(&CT->search_regex, strsearch, REG_EXTENDED | REG_ICASE);
		if(z != 0) {
			// FIXME The doc is unclear about whether a failed compilation requires free
			statusbar_set_error(CT, "Error regex: Invalid pattern");
			return;
		}
	}

	for(unsigned i=0; i<CT->items_nb; i++) {
		item_t* item = CT->items_arr + i;
		item->search_visible = FALSE;
		if(item->id==NULL) continue;
		if(item->content==NULL) continue;
		if(regex_mode==TRUE) {
			int z = regexec(&CT->search_regex, item->content, 0, NULL, 0);
			if(z==0) item->search_visible = TRUE;
		}
		else {
			if(strcasestr(item->content, strsearch)!=0) item->search_visible = TRUE;
		}
	}

	if(regex_mode==TRUE) {
		regfree(&CT->search_regex);
	}
}



//================================================
// Handle display of tags and items
//================================================

static void tags_view_empty(control_t *CT) {
	do {
		GtkTreeIter iter;
		gboolean gb = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(CT->tags_list_store), &iter);
		if(gb==FALSE) break;
		gtk_list_store_remove(CT->tags_list_store, &iter);
	} while(1);
}

static void tags_view_update(control_t *CT) {
	GValue a = G_VALUE_INIT;

	GtkTreeIter iter;
	gboolean gb = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(CT->tags_list_store), &iter);
	while(gb==TRUE) {
		guint gidx;
		gtk_tree_model_get(GTK_TREE_MODEL(CT->tags_list_store), &iter, TAGS_COL_IDX, &gidx, -1);
		tag_t* tag = CT->tags_arr + gidx;

		if(tag->name == NULL) {
			gb = gtk_list_store_remove(CT->tags_list_store, &iter);
			continue;
		}

		g_value_init(&a, G_TYPE_STRING);
		g_value_set_string(&a, tag->name);
		gtk_list_store_set_value(CT->tags_list_store, &iter, TAGS_COL_NAME, &a);
		g_value_unset(&a);

		g_value_init(&a, G_TYPE_STRING);
		char buf[32] = { 0 };
		if(tag->has_counter == TRUE) sprintf(buf, "%u", tag->counter);
		g_value_set_string(&a, buf);
		gtk_list_store_set_value(CT->tags_list_store, &iter, TAGS_COL_COUNT, &a);
		g_value_unset(&a);

		gb = gtk_tree_model_iter_next(GTK_TREE_MODEL(CT->tags_list_store), &iter);
	}

}

static void items_view_update_iter(control_t *CT, item_t* item, GtkTreeIter *piter) {
	GValue a = G_VALUE_INIT;

	// Set item ID
	g_value_init(&a, G_TYPE_STRING);
	if(item->id != NULL) g_value_set_string(&a, item->id);
	else g_value_set_static_string(&a, "???");
	gtk_list_store_set_value(CT->filt_list_store, piter, FILT_COL_ID, &a);
	g_value_unset(&a);

	// Set item title
	g_value_init(&a, G_TYPE_STRING);
	if(item->title != NULL) g_value_set_string(&a, item->title);
	else g_value_set_static_string(&a, "(empty)");
	gtk_list_store_set_value(CT->filt_list_store, piter, FILT_COL_TITLE, &a);
	g_value_unset(&a);
}

static void items_view_empty(control_t *CT) {
	do {
		GtkTreeIter iter;
		gboolean gb = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(CT->filt_list_store), &iter);
		if(gb==FALSE) break;
		gtk_list_store_remove(CT->filt_list_store, &iter);
	} while(1);
}

static void items_view_append(control_t *CT, item_t* item, GtkTreeIter *piter) {
	GtkTreeIter iter;
	if(piter==NULL) piter = &iter;

	// Acquire an iterator
	gtk_list_store_append(CT->filt_list_store, piter);
	GValue a = G_VALUE_INIT;

	// Set item index
	g_value_init(&a, G_TYPE_UINT);
	g_value_set_uint(&a, item->index);
	gtk_list_store_set_value(CT->filt_list_store, piter, FILT_COL_IDX, &a);
	g_value_unset(&a);

	// Set the rest of the content
	items_view_update_iter(CT, item, piter);
}

static void items_view_refresh(control_t *CT) {

	// Empty the list
	items_view_empty(CT);

	// Scan all items
	for(unsigned i=0; i<CT->items_nb; i++) {
		item_t* item = CT->items_arr + i;
		if(item->id==NULL) continue;
		if(item->content==NULL) continue;
		// Check if the item can be displayed
		if(item->search_visible==FALSE) continue;
		if(item->tags_visible==FALSE) continue;
		// Append the item to the view
		items_view_append(CT, item, NULL);
	}

}



//================================================
// Load/Save
//================================================

static void file_load_errmsg_with_code(control_t *CT, const char* msg, int code) {
	char buf[128];
	if(code < 0) {
		sprintf(buf, "Loading error: %s", msg);
	}
	else {
		sprintf(buf, "Loading error: %s (code %u)", msg, code);
	}
	printf("%s\n", buf);
	statusbar_set_error(CT, buf);
}

static void file_load(control_t *CT) {
	unsigned error_id = 0;

	if(CT->filename == NULL) goto EPILOGUE;

	FILE* F = fopen(CT->filename, "rb");
	if(F==NULL) {
		printf("Loading error: Could not open file: %s\n", CT->filename);
		statusbar_set_error(CT, "Loading error: Could not open file");
		error_id = __LINE__;
		goto EPILOGUE;
	}

	size_t linebuf_size = 2048;
	char* linebuf = malloc(linebuf_size);

	char buf[128];
	char markerbuf[32];
	ssize_t s;

	// Get the format version (first line)
	s = getline(&linebuf, &linebuf_size, F);
	if(s < 0) {
		file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__);
		goto CLEAN;
	}
	sprintf(buf, "%s\n", FILE_VERSION);
	if(strcmp(linebuf, buf)!=0) {
		file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__);
		goto CLEAN;
	}

	#define READWORD(buf) do { \
		buf[0] = 0; \
		fscanf(F, "%31s", buf); \
	} while(0)

	#define READWORD_CHK(buf) do { \
		READWORD(buf); \
		if(buf[0]==0) { \
			file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__); \
			error_id = __LINE__; \
			goto CLEAN; \
		} \
	} while(0)

	// Get the content
	do {

		// FIXME Use getline and strsep() => have the line number and display it in messages

		READWORD(buf);
		if(buf[0]==0) break;

		if(strcmp(buf, "tagcount")==0) {
			// Get the tag name
			READWORD_CHK(buf);
			tag_t* tag = tag_get(CT, buf);
			if(tag!=NULL) {
				file_load_errmsg_with_code(CT, "Tag defined multiple times", __LINE__);
				error_id = __LINE__;
				goto CLEAN;
			}
			tag = tag_new(CT, NULL, buf, TAG_STATUS_NONE, TRUE);
			// Get the tag counter
			READWORD_CHK(buf);
			tag->counter = atoi(buf);
			// Drop the end of the line
			s = getline(&linebuf, &linebuf_size, F);
			if(s < 0) {
				file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__);
				error_id = __LINE__;
				goto CLEAN;
			}
		}

		else if(strcmp(buf, "marker")==0) {
			READWORD_CHK(buf);
			if(CT->marker != NULL) g_free(CT->marker);
			CT->marker = g_strdup(buf);
		}

		else if(strcmp(buf, "description")==0) {
			// Get the marker
			markerbuf[0] = 0;
			fscanf(F, "%30s", markerbuf);
			if(markerbuf[0]==0) {
				file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__);
				error_id = __LINE__;
				goto CLEAN;
			}
			CT->desc_marker = g_strdup(markerbuf);
			unsigned markerlen = strlen(markerbuf);
			markerbuf[markerlen++] = '\n';
			markerbuf[markerlen] = 0;
			// Drop the end of the line
			s = getline(&linebuf, &linebuf_size, F);
			if(s < 0) {
				file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__);
				error_id = __LINE__;
				goto CLEAN;
			}
			// Get the description content
			unsigned desclen = 0;
			gchar* desc = NULL;
			do {
				s = getline(&linebuf, &linebuf_size, F);
				if(s < 0) {
					file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__);
					error_id = __LINE__;
					goto CLEAN;
				}
				if(strcmp(linebuf, markerbuf)==0) break;
				desc = g_realloc(desc, desclen + s + 1);
				strcpy(desc + desclen, linebuf);
				desclen += s;
			} while(1);
			// Save the description
			CT->desc_content = desc;
		}

		else if(strcmp(buf, "id")==0) {
			item_t* item = item_new(CT);
			// Get the ID
			READWORD_CHK(buf);
			item->id = g_strdup(buf);
			dbgprintf("ID: %s\n", item->id);
			// Get the tags
			READWORD_CHK(buf);
			if(strcmp(buf, "tags")!=0) {
				file_load_errmsg_with_code(CT, "Wrong token", __LINE__);
				error_id = __LINE__;
				goto CLEAN;
			}
			char* tags = NULL;
			int z = fscanf(F, "%ms", &tags);
			if(z != 1) {
				file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__);
				error_id = __LINE__;
				goto CLEAN;
			}
			dbgprintf("Tags: %s\n", tags);
			if(tags!=NULL) {
				item_tags_parse(CT, item, tags);
				free(tags);
			}
			// Get the marker
			markerbuf[0] = 0;
			fscanf(F, "%30s", markerbuf);
			if(markerbuf[0]==0) {
				file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__);
				error_id = __LINE__;
				goto CLEAN;
			}
			dbgprintf("Marker: %s\n", markerbuf);
			item->marker = g_strdup(markerbuf);
			unsigned markerlen = strlen(markerbuf);
			markerbuf[markerlen++] = '\n';
			markerbuf[markerlen] = 0;
			// Drop the end of the line
			s = getline(&linebuf, &linebuf_size, F);
			if(s < 0) {
				file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__);
				error_id = __LINE__;
				goto CLEAN;
			}
			// Get the description content
			unsigned len = 0;
			do {
				s = getline(&linebuf, &linebuf_size, F);
				if(s < 0) {
					file_load_errmsg_with_code(CT, "Unexpected end of file", __LINE__);
					error_id = __LINE__;
					goto CLEAN;
				}
				// Append the line to the content
				if(strcmp(linebuf, markerbuf)==0) break;
				item->content = g_realloc(item->content, (len + s + 1) * sizeof(*item->content));
				strcpy(item->content + len, linebuf);
				len += s;
			} while(1);
			// Get the title
			item_title_from_content(CT, item);
			// The item is now fully read
		}

		else {
			file_load_errmsg_with_code(CT, "Wrong token", __LINE__);
			error_id = __LINE__;
			goto CLEAN;
		}

	} while(1);

	// Write the number of items in the status bar
	sprintf(buf, "File loaded: %u items, %u tags", CT->items_nb, CT->tags_nb);
	//printf("%s\n", buf);
	statusbar_set(CT, buf);

	CLEAN:

	// Clean
	free(linebuf);
	fclose(F);

	EPILOGUE:

	// Mark the file as not modified
	CT->file_modified = FALSE;

	// Handle errors: clear the file name
	if(error_id != 0 && CT->filename != NULL) {
		g_free(CT->filename);
		CT->filename = NULL;
	}

	// Add the file name to the window title
	window_title_update(CT);

	// Need to update the view of tags because of the counters
	tags_view_update(CT);

	// Fill the item wiew
	items_view_refresh(CT);
}

static void file_save_choosefile(control_t* CT) {
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
	gint res;

	GtkWidget* dialog = gtk_file_chooser_dialog_new (
		"Save File", GTK_WINDOW(CT->window), action,
		"_Cancel", GTK_RESPONSE_CANCEL,
		"_Save", GTK_RESPONSE_ACCEPT,
		NULL
	);

	GtkFileChooser* chooser = GTK_FILE_CHOOSER(dialog);
	gtk_file_chooser_set_do_overwrite_confirmation(chooser, TRUE);

	if(CT->filename==NULL) {
		gtk_file_chooser_set_current_name(chooser, "Untitled document");
	}
	else {
		gtk_file_chooser_set_filename(chooser, CT->filename);
	}

	if(CT->filename != NULL) {
		g_free(CT->filename);
		CT->filename = NULL;
	}

	res = gtk_dialog_run(GTK_DIALOG(dialog));
	if(res==GTK_RESPONSE_ACCEPT) {
		CT->filename = gtk_file_chooser_get_filename(chooser);
	}

	gtk_widget_destroy(dialog);
}

static int file_save(control_t *CT) {

	// If no file name, open a dialog box so the user can select the file
	if(CT->filename == NULL) {
		file_save_choosefile(CT);
		window_title_update(CT);
		if(CT->filename == NULL) return 0;
	}

	// Open the file
	FILE* F = fopen(CT->filename, "wb");
	if(F==NULL) {
		printf("Error: Could not open file: %s\n", CT->filename);
		statusbar_set_error(CT, "Error: Could not open file");
		g_free(CT->filename);
		CT->filename = NULL;
		window_title_update(CT);
		return -1;
	}

	// Write the file version
	fprintf(F, "%s\n", FILE_VERSION);
	fputc('\n', F);

	// Write the tag counters
	unsigned counter_nb = 0;
	for(unsigned i=0; i<CT->tags_nb; i++) {
		tag_t* tag = CT->tags_arr + i;
		if(tag->name==NULL) continue;
		if(tag->has_counter==FALSE) continue;
		fprintf(F, "tagcount %s %u\n", tag->name, tag->counter);
		counter_nb++;
	}
	if(counter_nb > 0) fputc('\n', F);

	// Write the default marker
	fprintf(F, "marker %s\n", CT->marker);
	fputc('\n', F);

	// Write the description
	if(CT->desc_content != NULL) {
		fprintf(F, "description %s\n", CT->desc_marker);
		fprintf(F, "%s", CT->desc_content);
		fprintf(F, "%s\n", CT->desc_marker);
		fputc('\n', F);
	}

	// Write the items
	for(unsigned i=0; i<CT->items_nb; i++) {
		item_t* item = CT->items_arr + i;
		if(item->id == NULL) continue;
		if(item->content == NULL) continue;
		fprintf(F, "id %s tags ", item->id);
		unsigned tags_nb = 0;
		for(unsigned i=0; i<item->tags_nb; i++) {
			tag_t* tag = item->tags_arr[i];
			if(tag->name==NULL) continue;
			if(tags_nb > 0)  fprintf(F, ",");
			fprintf(F, "%s", item->tags_arr[i]->name);
			tags_nb++;
		}
		fprintf(F, " %s\n", item->marker);
		fprintf(F, "%s", item->content);
		fprintf(F, "%s\n", item->marker);
		fputc('\n', F);
	}

	// Clean
	fclose(F);

	//printf("File saved: %s\n", CT->filename);
	statusbar_set(CT, "File saved");

	// Mark the file as not modified
	CT->file_modified = FALSE;

	return 0;
}



//================================================
// Functions shared by callbacks
//================================================

// Remove all tags and redraw them
static void item_edit_updtags(control_t *CT) {
	GtkTextIter iterstart, iterend;

	// Remove all tags, if any
	gtk_text_buffer_get_bounds(CT->text_buffer, &iterstart, &iterend);
	gtk_text_buffer_remove_all_tags(CT->text_buffer, &iterstart, &iterend);

	// A buffer to store the currently scanned word
	const unsigned bufsize = 128;
	gchar gbuf[bufsize + 1];
	unsigned buflen = 0;

	gtk_text_buffer_get_start_iter(CT->text_buffer, &iterstart);
	gboolean end_reached = FALSE;

	// Scan all words of the text
	do {
		// Get a consistent beginning of word
		gboolean gb;
		gunichar c = 0;
		do {
			c = gtk_text_iter_get_char(&iterstart);
			if(c==0) break;
			if(g_ascii_isspace(c)==FALSE) break;
			// Next character
			gb = gtk_text_iter_forward_char(&iterstart);
			if(gb==FALSE) { c = 0; break; }
		} while(1);
		if(c==0) break;

		// Prepare to read the current word
		buflen = 0;
		gboolean buf_invalid = FALSE;
		iterend = iterstart;

		// Get the end of the current word
		do {
			c = gtk_text_iter_get_char(&iterend);
			if(c==0) { end_reached = TRUE; break; }
			if(g_ascii_isspace(c)==TRUE) break;
			// Add the character to the buffer
			if(buflen >= bufsize) buf_invalid = TRUE;
			else gbuf[buflen++] = c;
			// Next character
			gb = gtk_text_iter_forward_char(&iterend);
			if(gb==FALSE) { end_reached = TRUE; break; }
		} while(1);

		// Here we have a word. Find the item ID that corresponds, if any.
		if(buflen > 0 && buf_invalid==FALSE) {
			item_t* item = NULL;
			for(unsigned i=0; i<CT->items_nb; i++) {
				item_t* loc_item = CT->items_arr + i;
				if(loc_item->id==NULL) continue;
				if(strlen(loc_item->id)!=buflen) continue;
				if(g_ascii_strncasecmp(gbuf, loc_item->id, buflen)==0) { item = loc_item; break; }
			}
			// If an existing item is found, apply the tag
			if(item!=NULL) {
				gtk_text_buffer_apply_tag(CT->text_buffer, CT->text_tag_link, &iterstart, &iterend);
			}
		}

		// Begin search of next word there
		iterstart = iterend;

	} while(end_reached==FALSE);  // Scan all words of the text

}

static void item_edit_fill(control_t *CT, item_t* item) {
	// Clear the text view for security
	gtk_entry_set_text(GTK_ENTRY(CT->text_entry_id), "");
	gtk_entry_set_text(GTK_ENTRY(CT->text_entry_tags), "");
	gtk_text_buffer_set_text(CT->text_buffer, "", -1);

	if(item==NULL) return;

	// Fill the edition area with the item content

	if(item->id != NULL) {
		gtk_entry_set_text(GTK_ENTRY(CT->text_entry_id), item->id);
	}

	if(item->tags_nb > 0) {
		// Create a buffer to prepare the text
		unsigned len = 0;
		for(unsigned i=0; i<item->tags_nb; i++) {
			tag_t* tag = item->tags_arr[i];
			if(tag->name == NULL) continue;
			len += strlen(tag->name);
		}
		char buf[len + 2 * item->tags_nb + 10];
		// Prepare the text
		char* ptr = buf;
		unsigned tags_nb = 0;
		for(unsigned i=0; i<item->tags_nb; i++) {
			tag_t* tag = item->tags_arr[i];
			if(tag->name == NULL) continue;
			if(tags_nb > 0) { strcpy(ptr, ", "); ptr += 2; }
			strcpy(ptr, tag->name);
			ptr += strlen(tag->name);
			tags_nb++;
		}
		// Set the text
		gtk_entry_set_text(GTK_ENTRY(CT->text_entry_tags), buf);
	}

	if(item->content != NULL) {
		gtk_text_buffer_set_text(CT->text_buffer, item->content, -1);
		item_edit_updtags(CT);
	}
}

// Save the previously edited item (or delete it if empty)
static void item_save_selected(control_t *CT) {
	item_t* item = CT->item_selected;
	if(item == NULL) return;

	// Get the iterator from the view
	GtkTreeIter iter;
	gboolean iter_valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(CT->filt_list_store), &iter);
	while(iter_valid==TRUE) {
		guint gidx;
		gtk_tree_model_get(GTK_TREE_MODEL(CT->filt_list_store), &iter, FILT_COL_IDX, &gidx, -1);
		if(gidx==item->index) break;
		iter_valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(CT->filt_list_store), &iter);
	}

	// Get the buffer content
	GtkTextIter iterStart;
	GtkTextIter iterEnd;
	gtk_text_buffer_get_start_iter(CT->text_buffer, &iterStart);
	gtk_text_buffer_get_end_iter(CT->text_buffer, &iterEnd);
	gchar* gstr_content = gtk_text_buffer_get_text(CT->text_buffer, &iterStart, &iterEnd, FALSE);

	// Remove spaces at beginning and at the end. Append newline if non-empty.
	if(gstr_content!=NULL) {
		// Remove leading and trailing spaces
		g_strstrip(gstr_content);
		// Set the end with a newline
		if(*gstr_content != 0) {
			unsigned len = strlen(gstr_content);
			gstr_content = g_realloc(gstr_content, (len + 2) * sizeof(*gstr_content));
			gstr_content[len] = '\n';
			gstr_content[len+1] = 0;
		}
	}

	// Free the new content if nothing consistent
	if(gstr_content!=NULL && gstr_content[0]==0) {
		g_free(gstr_content);
		gstr_content = NULL;
	}

	// Delete the item if no content anymore
	if(gstr_content==NULL) {
		// Unset item fields
		if(item->id != NULL) g_free(item->id);
		item->id = NULL;
		if(item->content != NULL) g_free(item->content);
		item->content = NULL;
		if(item->title != NULL) g_free(item->title);
		item->title = NULL;
		// Remove item from the view
		if(iter_valid==TRUE) {
			gtk_list_store_remove(CT->filt_list_store, &iter);
			statusbar_set(CT, "Item removed because new content is empty");
		}
		// Mark the file as modified
		CT->file_modified = TRUE;
		return;
	}

	// From here the new content is not empty

	gboolean item_modified = FALSE;

	// Check if the content has been modified
	gboolean content_modified = FALSE;
	if(item->content==NULL || strcmp(item->content, gstr_content)!=0) {
		content_modified = TRUE;
		item_modified = TRUE;
	}

	// Save the new content in the item
	if(content_modified==TRUE) {
		item->content = gstr_content;
		item_title_from_content(CT, item);
	}

	// Ensure the marker is still valid = is not already present in the content
	// If conflict, generate a new marker: <marker><integer> and test integers
	if(content_modified==TRUE && marker_possible(item->content, item->marker)==FALSE) {
		unsigned len = strlen(CT->marker);
		char buf[len + 32];
		strcpy(buf, CT->marker);
		for(unsigned i=0; ; i++) {
			if(marker_possible(item->content, buf)==TRUE) break;
			sprintf(buf + len, "%u", i);
		}
		g_free(item->marker);
		item->marker = g_strdup(buf);
	}

	// Get the flags
	// Save previous flags to check if modified
	unsigned prev_tags_nb = item->tags_nb;
	tag_t* prev_tags[prev_tags_nb];
	memcpy(prev_tags, item->tags_arr, prev_tags_nb * sizeof(*item->tags_arr));
	// Convert new flags
	const gchar* const_gstr_flags = gtk_entry_get_text(GTK_ENTRY(CT->text_entry_tags));
	gchar* gstr_flags = g_strdup(const_gstr_flags);
	item_tags_parse(CT, item, gstr_flags);
	if(gstr_flags!=NULL) g_free(gstr_flags);
	// Check if modified
	if(prev_tags_nb != item->tags_nb) item_modified = TRUE;
	else if(prev_tags_nb > 0) {
		if(memcmp(item->tags_arr, prev_tags, prev_tags_nb * sizeof(*item->tags_arr))!=0) item_modified = TRUE;
	}

	// Get the ID. If modified, it should be a tag name => build a new item ID from the tag counter.
	const gchar* const_gstr_id = gtk_entry_get_text(GTK_ENTRY(CT->text_entry_id));
	gchar* gstr_id = g_strdup(const_gstr_id);
	if(gstr_id!=NULL) {
		gstr_id = g_strstrip(gstr_id);
		if(gstr_id[0]==0) { g_free(gstr_id); gstr_id = NULL; }
	}
	// Here the new ID is filtered. Check if the item has to be removed.
	if(gstr_id==NULL) {
		// Unset item fields
		if(item->id != NULL) g_free(item->id);
		item->id = NULL;
		if(item->content != NULL) g_free(item->content);
		item->content = NULL;
		if(item->title != NULL) g_free(item->title);
		item->title = NULL;
		// Remove the item from the view
		if(iter_valid==TRUE) {
			gtk_list_store_remove(CT->filt_list_store, &iter);
			statusbar_set(CT, "Item removed because new ID is empty");
		}
		// Mark the file as modified
		CT->file_modified = TRUE;
		return;
	}
	// Here the new ID is non-empty, check it
	if(item->id != NULL) {
		if(strcmp(item->id, gstr_id)==0) { g_free(gstr_id); gstr_id = NULL; }
	}
	// Here if the new ID is non-NULL, it means we want to set it to the item
	if(gstr_id != NULL) {
		tag_t* tag = tag_get(CT, gstr_id);
		g_free(gstr_id); gstr_id = NULL;
		if(tag==NULL) {
			statusbar_set_error(CT, "Item error: The new ID must be an existing tag");
		}
		else if(tag->has_counter == FALSE) {
			statusbar_set_error(CT, "Item error: The new ID must be a tag with a counter");
			tag = NULL;
		}
		// Here if tag is still non-NULL, it is a valid tag with a counter => build a new item ID
		if(tag!=NULL) {
			item_modified = TRUE;
			// Create a new tag ID
			char buf[strlen(tag->name) + 16];
			sprintf(buf, "%s%u", tag->name, tag->counter++);
			if(item->id != NULL) g_free(item->id);
			item->id = g_strdup(buf);
			// Update the tag view because the counter has changed
			tags_view_update(CT);
			// Add the tag to the item tags
			item_tag_add(CT, item, tag->name);
		}
	}

	// Finally, update the view for this item
	if(iter_valid==TRUE) {
		items_view_update_iter(CT, item, &iter);
	}

	// Update the edit view, unconditionally (tags may have to be re-formatted)
	GtkTreeModel *model = NULL;
	gboolean gb = gtk_tree_selection_get_selected(CT->filt_selection, &model, &iter);
	if(gb==TRUE) {
		guint gidx = 0;
		gtk_tree_model_get(GTK_TREE_MODEL(CT->filt_list_store), &iter, FILT_COL_IDX, &gidx, -1);
		if(gidx==item->index) {
			item_edit_fill(CT, item);
		}
	}

	// If successfully edited, print a message in status bar
	if(item_modified==TRUE) {
		statusbar_set(CT, "Item edited successfully");
		// Mark the file as modified
		CT->file_modified = TRUE;
	}
}

static void item_save_selected_scroll(control_t *CT) {
	if(CT->item_selected==NULL) return;

	// Save the data
	item_save_selected(CT);

	// Scroll the view when sorting is enabled, in case the row position changed
	if(CT->filt_sorting==SORT_NONE) return;

	GtkTreeModel *model = NULL;
	GtkTreeIter iter;
	gboolean gb = gtk_tree_selection_get_selected(CT->filt_selection, &model, &iter);
	if(gb==TRUE) {
		GtkTreePath* path = gtk_tree_model_get_path(GTK_TREE_MODEL(CT->filt_list_store), &iter);
		gtk_tree_view_set_cursor(GTK_TREE_VIEW(CT->filt_tree_view), path, NULL, FALSE);
		gtk_tree_path_free(path);
	}
}

// Display a simple OK/CANCEL dialog box that asks something
static gint dialog_okcancel(control_t *CT, gchar* msg) {
	// Create the dialog box
	GtkWidget *dialog = gtk_message_dialog_new(
		GTK_WINDOW(CT->window),
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_QUESTION,
		GTK_BUTTONS_OK_CANCEL,
		msg
	);
	// Run and get the response
	gint res = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	return res;
}



//================================================
// Callback functions
//================================================

// Callbacks for tags

// Toggle the state of a checkbox
static void callback_tags_name_edited(GtkCellRendererText* cell_renderer, gchar* path, gchar* new_text, control_t *CT) {

	// Get the iterator
	GtkTreeIter iter;
	gboolean gb = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(CT->tags_list_store), &iter, path);
	if(gb==FALSE) {
		return;
	}

	// Get the tag index
	guint gidx;
	gtk_tree_model_get(GTK_TREE_MODEL(CT->tags_list_store), &iter, TAGS_COL_IDX, &gidx, -1);
	tag_t* tag = CT->tags_arr + gidx;

	// Strip leading and trailing spaces
	if(new_text!=NULL) {
		g_strstrip(new_text);
		if(new_text[0]==0) {
			new_text = NULL;
		}
	}

	// The new tag name must not be empty
	if(new_text==NULL) {
		statusbar_set_error(CT, "Tag error: Rename requires a valid new name");
		return;
	}

	// From here the new tag name is non-empty

	// The new name must not contain spaces nor commas
	unsigned len = strcspn(new_text, SPACECOMMA);
	if(new_text[len]!=0) {
		statusbar_set_error(CT, "Tag error: A tag name can't contain spaces nor commas");
		return;
	}

	// From here we have a valid new name

	// Check if name is actually modified
	if(tag->name != NULL) {
		if(strcmp(new_text, tag->name)==0) {
			statusbar_set(CT, "Tag name unchanged");
			return;
		}
	}

	// Forbid renaming tags with counter
	if(tag->has_counter == TRUE && tag->counter > 0) {
		statusbar_set_error(CT, "Tag error: A tag with counter can't be renamed");
		return;
	}

	// Check unicity of tag names
	tag_t* othertag = tag_get(CT, new_text);
	if(othertag!=NULL && othertag!=tag) {

		// There is conflict, ask to merge
		gint res = dialog_okcancel(CT, "The new tag name already exists. Merge?");
		if(res!=GTK_RESPONSE_OK) {
			statusbar_set(CT, "Tag name unchanged");
			return;
		}

		// Save the currently selected item, in case it has the tag
		item_save_selected(CT);

		// Unlink the tag from all items, replace by the other item
		unsigned modif_nb = 0;
		for(unsigned i=0; i<CT->items_nb; i++) {
			item_t* item = CT->items_arr + i;
			unsigned cur_modif_nb = item_tag_remove(CT, item, tag);
			if(cur_modif_nb > 0) {
				int idx = item_tag_getidx(CT, item, othertag);
				if(idx < 0) item->tags_arr[item->tags_nb++] = othertag;
			}
			modif_nb += cur_modif_nb;
			// Refresh the currently selected item, if something was modified
			if(CT->item_selected==item && cur_modif_nb > 0) {
				item_edit_fill(CT, CT->item_selected);
			}
		}

		// Invalidate the tag
		if(tag->name != NULL) { g_free(tag->name); tag->name = NULL; }

		// Remove the tag from the view
		gtk_list_store_remove(CT->tags_list_store, &iter);

		// Mark the file as modified
		if(modif_nb > 0) CT->file_modified = TRUE;

		statusbar_set(CT, "Tag merged");

		return;
	}

	// Save the currently selected item, in case it has the tag
	item_save_selected(CT);

	// Set the new name
	if(tag->name != NULL) g_free(tag->name);
	// Note: It seems we can't "take" the string passed as argument (internal buffer?)
	// So it is duplicated here
	tag->name = g_strdup(new_text);

	// Refresh the currently selected item
	item_edit_fill(CT, CT->item_selected);

	// Update the view
	tags_view_update(CT);

	statusbar_set(CT, "Tag successfully edited");
}

// Click on name column: change sorting order
static void callback_tags_col_name(GtkWidget *widget, control_t *CT) {
	// NONE -> INC -> DEC -> NONE
	if(CT->tags_sorting==SORT_INC) CT->tags_sorting = SORT_DEC;
	else if(CT->tags_sorting==SORT_DEC) CT->tags_sorting = SORT_NONE;
	else CT->tags_sorting = SORT_INC;
	// Set the sorting indicator
	if(CT->tags_sorting==SORT_INC) {
		gtk_tree_view_column_set_sort_order(CT->tags_viewcol_name, GTK_SORT_ASCENDING);
		gtk_tree_view_column_set_sort_indicator(CT->tags_viewcol_name, TRUE);
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(CT->tags_list_store), TAGS_COL_NAME, GTK_SORT_ASCENDING);
	}
	else if(CT->tags_sorting==SORT_DEC) {
		gtk_tree_view_column_set_sort_order(CT->tags_viewcol_name, GTK_SORT_DESCENDING);
		gtk_tree_view_column_set_sort_indicator(CT->tags_viewcol_name, TRUE);
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(CT->tags_list_store), TAGS_COL_NAME, GTK_SORT_DESCENDING);
	}
	else {
		gtk_tree_view_column_set_sort_indicator(CT->tags_viewcol_name, FALSE);
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(CT->tags_list_store), TAGS_COL_IDX, GTK_SORT_ASCENDING);
	}
	// Ask to re-sort the list
	gtk_tree_sortable_sort_column_changed(GTK_TREE_SORTABLE(CT->tags_list_store));
}

// Toggle one checkbox
static void callback_tags_toggled(control_t *CT, gchar* path, unsigned col_id, unsigned other_col_id) {
	// Get the iterator
	GtkTreeIter iter;
	gboolean gb = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(CT->tags_list_store), &iter, path);
	if(gb==FALSE) return;
	// Get the previous value
	gboolean val;
	gtk_tree_model_get(GTK_TREE_MODEL(CT->tags_list_store), &iter, col_id, &val, -1);

	// Decide what is the next value
	gboolean newval = TRUE;
	if(val==TRUE) newval = FALSE;

	// Update data structures
	unsigned new_status = TAG_STATUS_NONE;
	if(col_id==TAGS_COL_WITH && newval==TRUE) new_status = TAG_STATUS_WITH;
	if(col_id==TAGS_COL_WOUT && newval==TRUE) new_status = TAG_STATUS_WOUT;
	tag_set_status_fromiter(CT, &iter, new_status);

	// Filter again the items
	items_filter_tags(CT);
	items_view_refresh(CT);
}
static void callback_tags_with_toggled(GtkCellRendererToggle* cell_renderer, gchar* path, control_t *CT) {
	callback_tags_toggled(CT, path, TAGS_COL_WITH, TAGS_COL_WOUT);
}
static void callback_tags_wout_toggled(GtkCellRendererToggle* cell_renderer, gchar* path, control_t *CT) {
	callback_tags_toggled(CT, path, TAGS_COL_WOUT, TAGS_COL_WITH);
}

// Toggle the state of all checkboxes in columns
static void callback_tags_checkboxes(control_t *CT, unsigned col_id, unsigned other_col_id) {
	// Scan the column
	gboolean one_is_true = FALSE;
	gboolean one_is_false = FALSE;
	GtkTreeIter iter;
	gboolean gb = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(CT->tags_list_store), &iter);
	if(gb==FALSE) return;
	while(gb==TRUE) {
		gboolean val;
		gtk_tree_model_get(GTK_TREE_MODEL(CT->tags_list_store), &iter, col_id, &val, -1);
		if(val==TRUE) one_is_true = TRUE;
		if(val==FALSE) one_is_false = TRUE;
		gb = gtk_tree_model_iter_next(GTK_TREE_MODEL(CT->tags_list_store), &iter);
	}

	// Decide what state to set
	gboolean newval = TRUE;
	if(one_is_true==TRUE && one_is_false==FALSE) newval = FALSE;
	// Use GValue types
	GValue gval = G_VALUE_INIT;
	GValue gval_false = G_VALUE_INIT;
	g_value_init(&gval, G_TYPE_BOOLEAN);
	g_value_init(&gval_false, G_TYPE_BOOLEAN);
	g_value_set_boolean(&gval, newval);
	g_value_set_boolean(&gval_false, FALSE);

	// Set in the data structure
	unsigned new_status = TAG_STATUS_NONE;
	if(col_id==TAGS_COL_WITH && newval==TRUE) new_status = TAG_STATUS_WITH;
	if(col_id==TAGS_COL_WOUT && newval==TRUE) new_status = TAG_STATUS_WOUT;
	for(unsigned i=0; i<CT->tags_nb; i++) {
		tag_t* tag = CT->tags_arr + i;
		tag->status = new_status;
	}

	// Set the new value
	gb = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(CT->tags_list_store), &iter);
	while(gb==TRUE) {
		// Set in the view
		gtk_list_store_set_value(CT->tags_list_store, &iter, col_id, &gval);
		gtk_list_store_set_value(CT->tags_list_store, &iter, other_col_id, &gval_false);
		gb = gtk_tree_model_iter_next(GTK_TREE_MODEL(CT->tags_list_store), &iter);
	}
	// Clean
	g_value_unset(&gval);
	g_value_unset(&gval_false);

	// Filter again the items
	items_filter_tags(CT);
	items_view_refresh(CT);
}
static void callback_tags_col_with(GtkWidget *widget, control_t *CT) {
	callback_tags_checkboxes(CT, TAGS_COL_WITH, TAGS_COL_WOUT);
}
static void callback_tags_col_wout(GtkWidget *widget, control_t *CT) {
	callback_tags_checkboxes(CT, TAGS_COL_WOUT, TAGS_COL_WITH);
}

static gboolean callback_tags_keypress(GtkWidget *widget, GdkEvent* event, control_t *CT) {
	if(event->type != GDK_KEY_PRESS) return FALSE;
	GdkEventKey* eventkey = (GdkEventKey*)event;
	if(eventkey->keyval != GDK_KEY_Delete) return FALSE;

	// Get the selected row
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;
	gboolean gb = gtk_tree_selection_get_selected(CT->tags_selection, &model, &iter);
	if(gb==FALSE) return TRUE;

	// Get the tag data structure
	guint gidx = 0;
	gtk_tree_model_get(GTK_TREE_MODEL(CT->tags_list_store), &iter, TAGS_COL_IDX, &gidx, -1);
	tag_t* tag = CT->tags_arr + gidx;

	if(tag->has_counter == TRUE && tag->counter > 0) {
		statusbar_set_error(CT, "Tag error: Removal of tags with a non-zero counter is forbidden");
		return FALSE;
	}

	// Save the currently selected item, in case it has the tag
	item_save_selected(CT);

	// Unlink the tag from all items
	unsigned rem_nb = 0;
	for(unsigned i=0; i<CT->items_nb; i++) {
		item_t* item = CT->items_arr + i;
		unsigned cur_rem_nb = item_tag_remove(CT, item, tag);
		rem_nb += cur_rem_nb;
		// Refresh the currently selected item, if it had the tag
		if(CT->item_selected==item && cur_rem_nb > 0) {
			item_edit_fill(CT, CT->item_selected);
		}
	}

	// Invalidate the tag
	if(tag->name != NULL) { g_free(tag->name); tag->name = NULL; }

	// Remove the tag from the view
	gtk_list_store_remove(CT->tags_list_store, &iter);

	// Mark the file as modified
	if(rem_nb > 0) CT->file_modified = TRUE;

	statusbar_set(CT, "Tag removed");

	// Return TRUE to stop other handlers, or FALSE to transparently pass the event to other handlers
	return TRUE;
}

// Callbacks for list of items

// Click on ID column: change sorting order
static void callback_filt_col_id(GtkWidget *widget, control_t *CT) {
	// NONE -> INC -> DEC -> NONE
	if(CT->filt_sorting==SORT_INC) CT->filt_sorting = SORT_DEC;
	else if(CT->filt_sorting==SORT_DEC) CT->filt_sorting = SORT_NONE;
	else CT->filt_sorting = SORT_INC;
	// Set the sorting indicator
	if(CT->filt_sorting==SORT_INC) {
		gtk_tree_view_column_set_sort_order(CT->filt_viewcol_id, GTK_SORT_ASCENDING);
		gtk_tree_view_column_set_sort_indicator(CT->filt_viewcol_id, TRUE);
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(CT->filt_list_store), FILT_COL_ID, GTK_SORT_ASCENDING);
	}
	else if(CT->filt_sorting==SORT_DEC) {
		gtk_tree_view_column_set_sort_order(CT->filt_viewcol_id, GTK_SORT_DESCENDING);
		gtk_tree_view_column_set_sort_indicator(CT->filt_viewcol_id, TRUE);
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(CT->filt_list_store), FILT_COL_ID, GTK_SORT_DESCENDING);
	}
	else {
		gtk_tree_view_column_set_sort_indicator(CT->filt_viewcol_id, FALSE);
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(CT->filt_list_store), FILT_COL_IDX, GTK_SORT_ASCENDING);
	}
	// Ask to re-sort the list
	gtk_tree_sortable_sort_column_changed(GTK_TREE_SORTABLE(CT->filt_list_store));
}

static void callback_filt_cursor_changed(GtkTreeView *tree_view, control_t *CT) {
	item_t* newitem = NULL;

	// Get the newly selected item
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;
	gboolean gb = gtk_tree_selection_get_selected(CT->filt_selection, &model, &iter);
	if(gb==TRUE) {
		guint gidx = 0;
		gtk_tree_model_get(GTK_TREE_MODEL(CT->filt_list_store), &iter, FILT_COL_IDX, &gidx, -1);
		newitem = CT->items_arr + gidx;
	}

	// Save the previously edited item
	if(CT->item_selected != NULL) {
		item_t* item = CT->item_selected;

		// Tiny optimization when the signal was emitted but nothing changed
		if(item==newitem) return;

		// Perform save
		item_save_selected(CT);

		// Clear pointer of selected item
		CT->item_selected = NULL;
	}

	// Make the edit area is non-editable, or editable
	if(newitem == NULL) {
		edit_area_disable(CT);
	}
	else {
		edit_area_enable(CT);
		// Fill the edition area with the newly selected item (if any)
		item_edit_fill(CT, newitem);
	}

	// Finally, set the new item as selected
	CT->item_selected = newitem;
}

static gboolean callback_filt_keypress(GtkWidget *widget, GdkEvent* event, control_t *CT) {
	if(event->type != GDK_KEY_PRESS) return FALSE;
	GdkEventKey* eventkey = (GdkEventKey*)event;
	if(eventkey->keyval != GDK_KEY_Delete) return FALSE;

	// Get the selected row
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;
	gboolean gb = gtk_tree_selection_get_selected(CT->filt_selection, &model, &iter);
	if(gb==FALSE) return TRUE;

	// Get the item data structure
	guint gidx = 0;
	gtk_tree_model_get(GTK_TREE_MODEL(CT->filt_list_store), &iter, FILT_COL_IDX, &gidx, -1);
	item_t* item = CT->items_arr + gidx;

	// Invalidate the item contents
	if(item->id != NULL) { g_free(item->id); item->id = NULL; }
	if(item->content != NULL) { g_free(item->content); item->content = NULL; }

	// Remove the item from the view
	gb = gtk_list_store_remove(CT->filt_list_store, &iter);
	if(gb==TRUE) {
		statusbar_set(CT, "Item removed");
	}

	// Mark the file as modified
	CT->file_modified = TRUE;

	// Return TRUE to stop other handlers, or FALSE to transparently pass the event to other handlers
	return TRUE;
}

// Callbacks for tags in text view

// Get the item whose ID is at the specified iterator
item_t* text_iter_get_word(control_t *CT, const GtkTextIter *iter, GtkTextIter *iterstart, GtkTextIter *iterend) {
	// Get the beginning of the word pointed at
	gboolean gb;
	gunichar c = 0;

	// Get a consistent beginning of word
	*iterstart = *iter;
	do {
		gb = gtk_text_iter_backward_char(iterstart);
		if(gb==FALSE) break;;
		c = gtk_text_iter_get_char(iterstart);
		if(g_ascii_isspace(c)==TRUE) {
			gtk_text_iter_forward_char(iterstart);
			break;
		}
	} while(1);

	// Get the end of the word
	const unsigned bufsize = 128;
	gchar gbuf[bufsize + 1];
	unsigned buflen = 0;
	gboolean buf_invalid = FALSE;

	*iterend = *iterstart;

	// Get the end of the current word
	do {
		c = gtk_text_iter_get_char(iterend);
		if(c==0) break;
		if(g_ascii_isspace(c)==TRUE) break;
		// Add the character to the buffer
		if(buflen >= bufsize) buf_invalid = TRUE;
		else gbuf[buflen++] = c;
		// Next character
		gb = gtk_text_iter_forward_char(iterend);
		if(gb==FALSE) break;
	} while(1);

	// Here we have a word. Find the item ID that corresponds, if any.
	if(buflen > 0 && buf_invalid==FALSE) {
		for(unsigned i=0; i<CT->items_nb; i++) {
			item_t* item = CT->items_arr + i;
			if(item->id==NULL) continue;
			if(strlen(item->id)!=buflen) continue;
			if(g_ascii_strncasecmp(gbuf, item->id, buflen)==0) return item;
		}
	}

	return NULL;
}

// When hovering a tag in the text view
gboolean callback_text_tag_event(GtkTextTag* tag, GObject *object, GdkEvent *event, GtkTextIter *iter, control_t *CT) {
	//printf("Event %u\n", event->type);

	// Hover item ID, display a tooltip to show the item title
	if(event->type==GDK_MOTION_NOTIFY) {

		// Get the item whose ID was pointed at
		GtkTextIter iterstart, iterend;
		item_t* item = text_iter_get_word(CT, iter, &iterstart, &iterend);

		// If no item is found, it means the text was modified and the tag is no longer valid -> update tags
		if(item == NULL) {
			item_edit_updtags(CT);
		}
		else {
			// FIXME Display a tooltip to show the item title
		}

	}  // Hover item ID

	// Double click: change selection to the corresponding item
	if(event->type==GDK_2BUTTON_PRESS) {

		// Get the item whose ID was pointed at
		GtkTextIter iterstart, iterend;
		item_t* item = text_iter_get_word(CT, iter, &iterstart, &iterend);

		if(item!=NULL) {
			// Unselect all, in case we had a selected item
			gtk_tree_selection_unselect_all(CT->filt_selection);
			// Save the current item, in case the previous item was not selected already
			item_save_selected(CT);

			// Get the iterator from the view
			GtkTreeIter iter;
			gboolean iter_valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(CT->filt_list_store), &iter);
			while(iter_valid==TRUE) {
				guint gidx;
				gtk_tree_model_get(GTK_TREE_MODEL(CT->filt_list_store), &iter, FILT_COL_IDX, &gidx, -1);
				if(gidx==item->index) break;
				iter_valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(CT->filt_list_store), &iter);
			}

			// Activate edition in edit area
			edit_area_enable(CT);

			// Scroll the view to show the row
			if(iter_valid==TRUE) {
				gtk_tree_selection_select_iter(CT->filt_selection, &iter);
				GtkTreePath* path = gtk_tree_model_get_path(GTK_TREE_MODEL(CT->filt_list_store), &iter);
				gtk_tree_view_set_cursor(GTK_TREE_VIEW(CT->filt_tree_view), path, NULL, FALSE);
				gtk_tree_path_free(path);
			}
			else {
				// No selection: manually fill the next item
				item_edit_fill(CT, item);
			}

			return TRUE;
		}

	}  // Double click on item ID

	// Return TRUE to stop other handlers from being invoked for the event
	// Return FALSE to propagate the event further
	return FALSE;
}

// When the text has changed in the text view
static void callback_text_changed(control_t *CT) {
	static gboolean first_run = TRUE;
	static gint64 prev_time = 0;

	// Get the current time in microseconds
	gint64 cur_time = g_get_monotonic_time();

	// Run updates of tags in text at most every second, because it can be costly
	if(first_run==TRUE) {
		prev_time = cur_time;
		first_run = FALSE;
	}
	else {
		if(cur_time - prev_time < 1000 * 1000) return;
	}
	prev_time = cur_time;

	// Update the tags in the text view
	item_edit_updtags(CT);
}

// Callbacks for tooltips in edit area

gboolean callback_text_query_tooltip(GtkWidget* widget, gint x, gint y, gboolean keyboard_mode, GtkTooltip *tooltip, control_t *CT) {
	// Note: No need to handle keyboard events
	// Because when typing, the tooltip disappears (maybe because keyboard mode is ignored?)
	if(keyboard_mode==TRUE) return FALSE;

	int mouse_x, mouse_y, trailing;
	GtkTextIter iter;
	gboolean gb;

	gtk_text_view_window_to_buffer_coords(GTK_TEXT_VIEW(CT->text_edit), GTK_TEXT_WINDOW_TEXT, x, y, &mouse_x, &mouse_y);
	gb = gtk_text_view_get_iter_at_position(GTK_TEXT_VIEW(CT->text_edit), &iter, &trailing, mouse_x, mouse_y);
	if(gb==FALSE) return FALSE;

	// Here the iterator is over some text
	GtkTextIter iterstart, iterend;
	item_t* item = text_iter_get_word(CT, &iter, &iterstart, &iterend);
	if(item==NULL) return FALSE;
	if(item->title==NULL) return FALSE;

	// Set the tooltip text: the item title
	gtk_tooltip_set_text(tooltip, item->title);

	// If a tooltip should be shown, return TRUE. Otherwise, return FALSE.
	return TRUE;
}

// Callbacks for menus and accelerators

static void callback_tag_new_internal(control_t *CT, gboolean with_counter) {
	char buf[128];

	// Generate a new tag name
	for(unsigned i=1; ; i++) {
		sprintf(buf, "newtag%u", i);
		tag_t* tag = tag_get(CT, buf);
		if(tag==NULL) break;
	}

	// Add the new tag
	GtkTreeIter iter;
	tag_new(CT, &iter, g_strdup(buf), TAG_STATUS_NONE, with_counter);

	// Scroll the view to show the row
	GtkTreePath* path = gtk_tree_model_get_path(GTK_TREE_MODEL(CT->tags_list_store), &iter);
	gtk_tree_view_set_cursor(GTK_TREE_VIEW(CT->tags_tree_view), path, CT->tags_viewcol_name, TRUE);
	gtk_tree_path_free(path);
}
static void callback_tag_new_nocounter(control_t *CT) {
	callback_tag_new_internal(CT, FALSE);
}
static void callback_tag_new_counter(control_t *CT) {
	callback_tag_new_internal(CT, TRUE);
}

static void callback_item_new(control_t *CT) {
	item_t* item = item_new(CT);

	// Note: the new item intentionally has no ID and no tags

	// Fill the new item with dummy content
	item->content = g_strdup("<New title>\n<New content>");
	item_title_from_content(CT, item);

	// Append a new line in the view of items
	// (independently of filtering options)
	GtkTreeIter iter;
	items_view_append(CT, item, &iter);

	// Force the new item as selected
	gtk_tree_selection_select_iter(CT->filt_selection, &iter);

	// Scroll the view to show the row
	GtkTreePath* path = gtk_tree_model_get_path(GTK_TREE_MODEL(CT->filt_list_store), &iter);
	gtk_tree_view_set_cursor(GTK_TREE_VIEW(CT->filt_tree_view), path, NULL, FALSE);
	gtk_tree_path_free(path);

	// Set focus to the entry to edit ID
	gtk_widget_grab_focus(CT->text_entry_id);
}

static void callback_open(control_t *CT) {

	// Save the item being edited
	item_save_selected_scroll(CT);

	// If there were modifications, ask confirmation to the user
	if(CT->file_modified==TRUE) {
		gint res = dialog_okcancel(CT, "There are unsaved modifications. Overwrite anyway?");
		if(res!=GTK_RESPONSE_OK) return;
	}

	// Create a dialog box to select a new file
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
	gint res;

	GtkWidget* dialog = gtk_file_chooser_dialog_new (
		"Load File", GTK_WINDOW(CT->window), action,
		"_Cancel", GTK_RESPONSE_CANCEL,
		"_Open", GTK_RESPONSE_ACCEPT,
		NULL
	);

	GtkFileChooser* chooser = GTK_FILE_CHOOSER(dialog);

	if(CT->filename != NULL) {
		gtk_file_chooser_set_filename(chooser, CT->filename);
	}

	gchar* new_filename = NULL;
	res = gtk_dialog_run(GTK_DIALOG(dialog));
	if(res==GTK_RESPONSE_ACCEPT) {
		new_filename = gtk_file_chooser_get_filename(chooser);
	}

	gtk_widget_destroy(dialog);

	if(new_filename==NULL) return;

	// Update the file name
	if(CT->filename != NULL) {
		g_free(CT->filename);
	}
	CT->filename = new_filename;

	// Empty views of tags and items
	tags_view_empty(CT);
	items_view_empty(CT);

	// Invalidate current tags and items
	for(unsigned i=0; i<CT->tags_nb; i++) {
		tag_t* tag = CT->tags_arr + i;
		if(tag->name != NULL) { g_free(tag->name); tag->name = NULL; }
	}
	CT->tags_nb = 0;
	for(unsigned i=0; i<CT->items_nb; i++) {
		item_t* item = CT->items_arr + i;
		if(item->id != NULL) { g_free(item->id); item->id = NULL; }
	}
	CT->items_nb = 0;

	// Clear the modification flag
	CT->file_modified = FALSE;

	// Load the file
	file_load(CT);
}

static void callback_save(control_t *CT) {
	// FIXME Check unicity of IDs, tags, etc. All must be lowercase.
	// FIXME If something wrong is found, set msg in status bar, open a dialog box and do not save

	// Save the item being edited
	item_save_selected_scroll(CT);

	// Write the file
	file_save(CT);
}

// Launch search
static void callback_search(control_t *CT) {
	// Get the text to search
	const gchar* strsearch = gtk_entry_get_text(GTK_ENTRY(CT->search_entry));
	// Filter again the items
	items_filter_search(CT, strsearch);
	items_view_refresh(CT);
}

// Just set focus on search entry
static void callback_accel_search(control_t *CT) {
	gtk_widget_grab_focus(GTK_WIDGET(CT->search_entry));
}

static void callback_refresh(control_t *CT) {

	// Clear item numbers in tags
	for(unsigned i=0; i<CT->tags_nb; i++) {
		tag_t* tag = CT->tags_arr + i;
		tag->items_nb = 0;
	}
	// Count items for all tags
	for(unsigned i=0; i<CT->items_nb; i++) {
		item_t* item = CT->items_arr + i;
		for(unsigned t=0; t<item->tags_nb; t++) {
			tag_t* tag = item->tags_arr[t];
			tag->items_nb ++;
		}
	}
	// Invalidate unused tags, except if they have a counter
	for(unsigned i=0; i<CT->tags_nb; i++) {
		tag_t* tag = CT->tags_arr + i;
		if(tag->name == NULL) continue;
		if(tag->has_counter == TRUE) continue;
		if(tag->items_nb > 0) continue;
		g_free(tag->name);
		tag->name = NULL;
	}

	tags_view_update(CT);
	items_view_refresh(CT);
}

// Callbacks for main window

static void callback_about(control_t *CT) {
	// Create the dialog box
	GtkWidget *dialog = gtk_message_dialog_new(
		GTK_WINDOW(CT->window),
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO,
		GTK_BUTTONS_OK,
		"This is " GITODOUBLE_NAME " version " GITODOUBLE_VERSION "\n"
		"By " GITODOUBLE_AUTHOR "\n"
		"\n"
		"This program is free software.\n"
		"It comes with ABSOLUTELY NO WARRANTY.\n"
	);
	gtk_window_set_title(GTK_WINDOW(dialog), "About");
	// Run and destroy
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

static gboolean callback_quit(control_t *CT) {
	// Save the item being edited
	item_save_selected(CT);

	// If there were modifications, ask confirmation to the user
	if(CT->file_modified==TRUE) {
		gint res = dialog_okcancel(CT, "There are unsaved modifications. Exit anyway?");
		if(res!=GTK_RESPONSE_OK) return TRUE;
	}

	// Indicate to GTK that it has to destroy all and quit
	gtk_main_quit();
	gtk_main_iteration();

	// return FALSE and the main window will be destroyed with a "delete_event".
	// Return TRUE if nothing has to be done.
	return FALSE;
}



//================================================
// Main function
//================================================

int main(int argc, char** argv) {

	// Create the main data structure

	control_t *CT = calloc(1, sizeof(*CT));
	if(CT==NULL) {
		printf("ERROR %s:%u: memory allocation.\n", __FILE__, __LINE__);
		exit(EXIT_FAILURE);
	}
	CT->marker = g_strdup(DEFAULT_MARKER);
	CT->file_modified = FALSE;

	// Parse command-line parameters

	for(unsigned i=1; i<argc; i++) {
		char* param = argv[i];
		if(strcmp(param, "--help")==0 || strcmp(param, "-h")==0) {
			printf(
				"Syntax: %s [options] [<file>]\n"
				"Options:\n"
				"  --help, -h        Display this help message and exit\n"
				"  --version         Display the version and exit\n",
				argv[0]
			);
			exit(EXIT_SUCCESS);
		}
		else if(strcmp(param, "--version")==0) {
			printf(GITODOUBLE_NAME " version " GITODOUBLE_VERSION "\n");
			printf("By " GITODOUBLE_AUTHOR "\n");
			printf("This program comes with ABSOLUTELY NO WARRANTY.\n");
			exit(EXIT_SUCCESS);
		}
		else if(param[0]=='-') {
			printf("Error: Unknown parameter '%s'\n", param);
			exit(EXIT_FAILURE);
		}
		else {
			if(CT->filename!=NULL) {
				printf("Error: Multiple file names are given, only one is authorized\n");
				exit(EXIT_FAILURE);
			}
			CT->filename = g_strdup(param);
		}
	}

	// Create the GUI

	// GTK+ Initialization
	gtk_init(NULL, NULL);

	// Misc utilities for tree views

	CT->cellrender_text = gtk_cell_renderer_text_new();

	// The list of tags

	CT->tags_render_name  = gtk_cell_renderer_text_new();
	CT->tags_render_count = gtk_cell_renderer_text_new();
	CT->tags_render_with  = gtk_cell_renderer_toggle_new();
	CT->tags_render_wout  = gtk_cell_renderer_toggle_new();

	GValue gval = G_VALUE_INIT;
	g_value_init(&gval, G_TYPE_BOOLEAN);
	g_value_set_boolean(&gval, TRUE);
	g_object_set_property(G_OBJECT(CT->tags_render_name), "editable", &gval);
	//g_object_set_property(G_OBJECT(CT->tags_render_count), "editable", &gval);
	g_value_unset(&gval);

	gtk_cell_renderer_toggle_set_activatable(GTK_CELL_RENDERER_TOGGLE(CT->tags_render_with), TRUE);
	gtk_cell_renderer_toggle_set_activatable(GTK_CELL_RENDERER_TOGGLE(CT->tags_render_wout), TRUE);

	g_signal_connect(G_OBJECT(CT->tags_render_name), "edited", G_CALLBACK(callback_tags_name_edited), CT);
	g_signal_connect(G_OBJECT(CT->tags_render_with), "toggled", G_CALLBACK(callback_tags_with_toggled), CT);
	g_signal_connect(G_OBJECT(CT->tags_render_wout), "toggled", G_CALLBACK(callback_tags_wout_toggled), CT);

	CT->tags_list_store = gtk_list_store_new(5, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_UINT);
	CT->tags_tree_view = gtk_tree_view_new_with_model(GTK_TREE_MODEL(CT->tags_list_store));
	gtk_widget_set_hexpand(CT->tags_tree_view, TRUE);
	gtk_widget_set_vexpand(CT->tags_tree_view, TRUE);

	CT->tags_viewcol_name = gtk_tree_view_column_new_with_attributes(
		"Tag",
		CT->tags_render_name,
		"text", TAGS_COL_NAME,
		NULL
	);
	CT->tags_viewcol_count = gtk_tree_view_column_new_with_attributes(
		"Count",
		CT->tags_render_count,
		"text", TAGS_COL_COUNT,
		NULL
	);
	CT->tags_viewcol_with = gtk_tree_view_column_new_with_attributes(
		"Y",
		CT->tags_render_with,
		"active", TAGS_COL_WITH,
		NULL
	);
	CT->tags_viewcol_wout = gtk_tree_view_column_new_with_attributes(
		"N",
		CT->tags_render_wout,
		"active", TAGS_COL_WOUT,
		NULL
	);

	gtk_tree_view_column_set_clickable(CT->tags_viewcol_name, TRUE);
	gtk_tree_view_column_set_clickable(CT->tags_viewcol_with, TRUE);
	gtk_tree_view_column_set_clickable(CT->tags_viewcol_wout, TRUE);

	g_signal_connect(G_OBJECT(CT->tags_viewcol_name), "clicked", G_CALLBACK(callback_tags_col_name), CT);
	g_signal_connect(G_OBJECT(CT->tags_viewcol_with), "clicked", G_CALLBACK(callback_tags_col_with), CT);
	g_signal_connect(G_OBJECT(CT->tags_viewcol_wout), "clicked", G_CALLBACK(callback_tags_col_wout), CT);

	// Overwrite the default sorting function: make sure "bug1" < "bug10" and "bug2" < "bug10"
	gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(CT->tags_list_store), TAGS_COL_NAME, treecmp_gstrcmp_fixnumbers, NULL, NULL);

	gtk_tree_view_column_set_expand(CT->tags_viewcol_name, TRUE);

	gtk_tree_view_append_column(GTK_TREE_VIEW(CT->tags_tree_view), CT->tags_viewcol_name);
	gtk_tree_view_append_column(GTK_TREE_VIEW(CT->tags_tree_view), CT->tags_viewcol_count);
	gtk_tree_view_append_column(GTK_TREE_VIEW(CT->tags_tree_view), CT->tags_viewcol_with);
	gtk_tree_view_append_column(GTK_TREE_VIEW(CT->tags_tree_view), CT->tags_viewcol_wout);

	CT->tags_selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(CT->tags_tree_view));
	gtk_tree_selection_set_mode(CT->tags_selection, GTK_SELECTION_SINGLE);

	CT->tags_scroll = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(CT->tags_scroll), CT->tags_tree_view);

	// Get key press events, for the suppr key
	g_signal_connect(G_OBJECT(CT->tags_tree_view), "key-press-event", G_CALLBACK(callback_tags_keypress), CT);

	// The list of filtered items

	CT->filt_list_store = gtk_list_store_new(3, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_UINT);
	CT->filt_tree_view = gtk_tree_view_new_with_model(GTK_TREE_MODEL(CT->filt_list_store));
	gtk_widget_set_hexpand(CT->filt_tree_view, TRUE);
	gtk_widget_set_vexpand(CT->filt_tree_view, TRUE);

	CT->filt_viewcol_id = gtk_tree_view_column_new_with_attributes(
		"ID",
		CT->cellrender_text,
		"text", FILT_COL_ID,
		NULL
	);
	CT->filt_viewcol_title = gtk_tree_view_column_new_with_attributes(
		"Title",
		CT->cellrender_text,
		"text", FILT_COL_TITLE,
		NULL
	);

	gtk_tree_view_column_set_clickable(CT->filt_viewcol_id, TRUE);
	g_signal_connect(G_OBJECT(CT->filt_viewcol_id), "clicked", G_CALLBACK(callback_filt_col_id), CT);

	// Overwrite the default sorting function: make sure "bug1" < "bug10" and "bug2" < "bug10"
	gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(CT->filt_list_store), FILT_COL_ID, treecmp_gstrcmp_fixnumbers, NULL, NULL);

	gtk_tree_view_column_set_expand(CT->filt_viewcol_title, TRUE);

	gtk_tree_view_append_column(GTK_TREE_VIEW(CT->filt_tree_view), CT->filt_viewcol_id);
	gtk_tree_view_append_column(GTK_TREE_VIEW(CT->filt_tree_view), CT->filt_viewcol_title);

	CT->filt_selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(CT->filt_tree_view));
	gtk_tree_selection_set_mode(CT->filt_selection, GTK_SELECTION_SINGLE);
	g_signal_connect(G_OBJECT(CT->filt_selection), "changed", G_CALLBACK(callback_filt_cursor_changed), CT);

	CT->filt_scroll = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(CT->filt_scroll), CT->filt_tree_view);

	// Get key press events, for the suppr key
	g_signal_connect(G_OBJECT(CT->filt_tree_view), "key-press-event", G_CALLBACK(callback_filt_keypress), CT);

	// The item edit area

	CT->text_edit = gtk_text_view_new();
	gtk_text_view_set_monospace(GTK_TEXT_VIEW(CT->text_edit), TRUE);
	gtk_widget_set_hexpand(CT->text_edit, TRUE);
	gtk_widget_set_vexpand(CT->text_edit, TRUE);
	CT->text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(CT->text_edit));

	// A tag to highlight item IDs in text
	CT->text_tag_link = gtk_text_buffer_create_tag(CT->text_buffer, "linktag",
		"weight", PANGO_WEIGHT_BOLD,
		"underline", PANGO_UNDERLINE_SINGLE,
		NULL
	);
	g_signal_connect(G_OBJECT(CT->text_tag_link), "event", G_CALLBACK(callback_text_tag_event), CT);

	// Set this signal after the TextBuffer tag is defined, otherwise there is a risk of segfault
	g_signal_connect_swapped(G_OBJECT(CT->text_buffer), "changed", G_CALLBACK(callback_text_changed), CT);

	// Callback handler to show tooltip in edit area: when the currently hovered word is a valid item ID
	g_signal_connect(G_OBJECT(CT->text_edit), "query-tooltip", G_CALLBACK(callback_text_query_tooltip), CT);
	gtk_widget_set_has_tooltip(CT->text_edit, TRUE);

	CT->text_scroll = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(CT->text_scroll), CT->text_edit);

	CT->text_entry_id = gtk_entry_new();
	CT->text_entry_tags = gtk_entry_new();

	gtk_widget_set_tooltip_text(CT->text_entry_id, "Give the name of a tag that has a counter,\nand the item ID will be automatically generated");
	gtk_widget_set_tooltip_text(CT->text_entry_tags, "Give a list of tag names, separated by spaces or commas");

	// Forbid newline inside text when pasting stuff in ID and flags
	g_value_init(&gval, G_TYPE_BOOLEAN);
	g_value_set_boolean(&gval, TRUE);
	g_object_set_property(G_OBJECT(CT->text_entry_id), "truncate-multiline", &gval);
	g_object_set_property(G_OBJECT(CT->text_entry_id), "truncate-multiline", &gval);
	g_value_unset(&gval);

	CT->text_hpaned_entries = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_paned_set_wide_handle(GTK_PANED(CT->text_hpaned_entries), TRUE);

	gtk_paned_pack1(GTK_PANED(CT->text_hpaned_entries), CT->text_entry_id, TRUE, FALSE);
	gtk_paned_pack2(GTK_PANED(CT->text_hpaned_entries), CT->text_entry_tags, TRUE, FALSE);

	CT->text_grid = gtk_grid_new();
	gtk_grid_set_row_spacing(GTK_GRID(CT->text_grid), SPACING_PIX);
	gtk_grid_set_column_spacing(GTK_GRID(CT->text_grid), SPACING_PIX);

	gtk_grid_attach_next_to(GTK_GRID(CT->text_grid), CT->text_hpaned_entries, NULL, GTK_POS_BOTTOM, 1, 1);
	gtk_grid_attach_next_to(GTK_GRID(CT->text_grid), CT->text_scroll, NULL, GTK_POS_BOTTOM, 1, 1);

	// The status bar

	CT->statusbar = gtk_statusbar_new();
	gtk_widget_set_hexpand(CT->statusbar, TRUE);

	// The search bar

	CT->search_entry = gtk_entry_new();
	g_signal_connect_swapped(G_OBJECT(CT->search_entry), "activate", G_CALLBACK(callback_search), CT);

	// Forbid newline inside text when pasting stuff in ID and flags
	g_value_init(&gval, G_TYPE_BOOLEAN);
	g_value_set_boolean(&gval, TRUE);
	g_object_set_property(G_OBJECT(CT->search_entry), "truncate-multiline", &gval);
	g_value_unset(&gval);

	// Create a CSS context for the entire application
	// This enables to change text color, draw animations, etc
	// Documentation: https://people.gnome.org/~desrt/gtk/html/GtkCssProvider.html
	// Documentation: https://developer.gnome.org/gtk3/stable/chap-css-overview.html
	// Documentation: https://developer.gnome.org/gtk3/stable/gtk-migrating-GtkStyleContext-css.html
	// Documentation: https://developer.gnome.org/gtk3/stable/GtkStyleContext.html
	// Documentation: https://developer.gnome.org/gtk3/stable/GtkCssProvider.html

	GdkDisplay* display = gdk_display_get_default();
	GdkScreen* screen   = gdk_display_get_default_screen(display);
	CT->css_provider = gtk_css_provider_new();
	gtk_style_context_add_provider_for_screen(screen, GTK_STYLE_PROVIDER(CT->css_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

	g_object_unref(CT->css_provider);

	gtk_css_provider_load_from_data(CT->css_provider,
		"."CSS_STATUS_RED" { color: rgb(200,0,0); }\n"
		"."CSS_BG_GREY" * { color: rgb(180,180,180); }\n"
		, -1, NULL
	);

	// Option checkboxes

	CT->ckeckbox_search_regexp = gtk_check_button_new_with_label("Search is regexp");

	// Fill widgets in the window

	// Creation of the main window
	CT->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width(GTK_CONTAINER(CT->window), SPACING_PIX);

	// Function to call when the delete event is received
	g_signal_connect_swapped(G_OBJECT(CT->window), "delete_event", G_CALLBACK(callback_quit), CT);

	CT->grid = gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(CT->window), CT->grid);

	gtk_grid_set_row_spacing(GTK_GRID(CT->grid), SPACING_PIX);
	gtk_grid_set_column_spacing(GTK_GRID(CT->grid), SPACING_PIX);

	CT->hpaned = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
	CT->vpaned = gtk_paned_new(GTK_ORIENTATION_VERTICAL);
	gtk_paned_set_wide_handle(GTK_PANED(CT->hpaned), TRUE);
	gtk_paned_set_wide_handle(GTK_PANED(CT->vpaned), TRUE);

	CT->search_grid = gtk_grid_new();
	gtk_grid_set_row_spacing(GTK_GRID(CT->search_grid), SPACING_PIX);
	gtk_grid_set_column_spacing(GTK_GRID(CT->search_grid), SPACING_PIX);

	gtk_grid_attach_next_to(GTK_GRID(CT->search_grid), CT->search_entry, NULL, GTK_POS_BOTTOM, 1, 1);
	gtk_grid_attach_next_to(GTK_GRID(CT->search_grid), CT->ckeckbox_search_regexp, NULL, GTK_POS_BOTTOM, 1, 1);
	gtk_grid_attach_next_to(GTK_GRID(CT->search_grid), CT->tags_scroll, NULL, GTK_POS_BOTTOM, 1, 1);

	gtk_paned_pack1(GTK_PANED(CT->hpaned), CT->search_grid, TRUE, FALSE);
	gtk_paned_pack2(GTK_PANED(CT->hpaned), CT->vpaned, TRUE, FALSE);

	gtk_paned_pack1(GTK_PANED(CT->vpaned), CT->filt_scroll, TRUE, FALSE);
	gtk_paned_pack2(GTK_PANED(CT->vpaned), CT->text_grid, TRUE, FALSE);

	gtk_grid_attach_next_to(GTK_GRID(CT->grid), CT->hpaned, NULL, GTK_POS_BOTTOM, 1, 1);

	gtk_grid_attach_next_to(GTK_GRID(CT->grid), CT->statusbar, NULL, GTK_POS_BOTTOM, 1, 1);

	// Create keyboard shortcuts (accelerators)

	gtk_accel_map_add_entry(ACCMAP_OPEN,       GDK_KEY_O, GDK_CONTROL_MASK);
	gtk_accel_map_add_entry(ACCMAP_SAVE,       GDK_KEY_S, GDK_CONTROL_MASK);
	gtk_accel_map_add_entry(ACCMAP_QUIT,       GDK_KEY_Q, GDK_CONTROL_MASK);
	gtk_accel_map_add_entry(ACCMAP_NEWTAG,     GDK_KEY_T, GDK_CONTROL_MASK);
	gtk_accel_map_add_entry(ACCMAP_NEWTAG_CNT, GDK_KEY_T, GDK_CONTROL_MASK | GDK_SHIFT_MASK);
	gtk_accel_map_add_entry(ACCMAP_NEWITEM,    GDK_KEY_N, GDK_CONTROL_MASK);
	gtk_accel_map_add_entry(ACCMAP_REFRESH,    GDK_KEY_R, GDK_CONTROL_MASK);
	gtk_accel_map_add_entry(ACCMAP_SEARCH,     GDK_KEY_F, GDK_CONTROL_MASK);

	GtkAccelGroup* accel_group = gtk_accel_group_new();
	gtk_window_add_accel_group(GTK_WINDOW(CT->window), accel_group);

	GClosure* closure = NULL;

	closure = g_cclosure_new_swap(G_CALLBACK(callback_open), CT, NULL);
	gtk_accel_group_connect_by_path(accel_group, ACCMAP_OPEN, closure);

	closure = g_cclosure_new_swap(G_CALLBACK(callback_save), CT, NULL);
	gtk_accel_group_connect_by_path(accel_group, ACCMAP_SAVE, closure);

	closure = g_cclosure_new_swap(G_CALLBACK(callback_quit), CT, NULL);
	gtk_accel_group_connect_by_path(accel_group, ACCMAP_QUIT, closure);

	closure = g_cclosure_new_swap(G_CALLBACK(callback_tag_new_nocounter), CT, NULL);
	gtk_accel_group_connect_by_path(accel_group, ACCMAP_NEWTAG, closure);

	closure = g_cclosure_new_swap(G_CALLBACK(callback_tag_new_counter), CT, NULL);
	gtk_accel_group_connect_by_path(accel_group, ACCMAP_NEWTAG_CNT, closure);

	closure = g_cclosure_new_swap(G_CALLBACK(callback_item_new), CT, NULL);
	gtk_accel_group_connect_by_path(accel_group, ACCMAP_NEWITEM, closure);

	closure = g_cclosure_new_swap(G_CALLBACK(callback_refresh), CT, NULL);
	gtk_accel_group_connect_by_path(accel_group, ACCMAP_REFRESH, closure);

	closure = g_cclosure_new_swap(G_CALLBACK(callback_accel_search), CT, NULL);
	gtk_accel_group_connect_by_path(accel_group, ACCMAP_SEARCH, closure);

	// Add a menu

	GtkWidget* menubar = gtk_menu_bar_new();
	gtk_menu_bar_set_pack_direction(GTK_MENU_BAR(menubar), GTK_PACK_DIRECTION_LTR);
	gtk_menu_bar_set_child_pack_direction(GTK_MENU_BAR(menubar), GTK_PACK_DIRECTION_TTB);

	GtkWidget* menuitem = NULL;
	GtkWidget* submenu = NULL;

	submenu = gtk_menu_new();
	gtk_menu_set_accel_group(GTK_MENU(submenu), accel_group);

	menuitem = gtk_menu_item_new_with_mnemonic("_Open");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_OPEN);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_open), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("_Save");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_SAVE);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_save), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("_Quit");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_QUIT);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_quit), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("_File");
	gtk_menu_shell_append(GTK_MENU_SHELL(menubar), menuitem);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), submenu);

	submenu = gtk_menu_new();
	gtk_menu_set_accel_group(GTK_MENU(submenu), accel_group);

	menuitem = gtk_menu_item_new_with_mnemonic("New _tag");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_NEWTAG);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_tag_new_nocounter), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("New tag with _counter");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_NEWTAG_CNT);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_tag_new_counter), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("New _item");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_NEWITEM);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_item_new), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("_Refresh");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_REFRESH);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_refresh), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("_Content");
	gtk_menu_shell_append(GTK_MENU_SHELL(menubar), menuitem);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), submenu);

	menuitem = gtk_menu_item_new_with_mnemonic("_About");
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_about), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(menubar), menuitem);

	// Add the menubar to the window
	gtk_grid_attach_next_to(GTK_GRID(CT->grid), menubar, NULL, GTK_POS_TOP, 1, 1);

	// Initial welcome message in status bar

	statusbar_set(CT, "This is " GITODOUBLE_NAME " version " GITODOUBLE_VERSION);
	window_title_update(CT);

	// Initially, the edit area is non-editable because no item is selected
	edit_area_disable(CT);

	// Try to load the specified file

	file_load(CT);

	// Main GUI loop

	// Display the main window
	gtk_widget_show_all(CT->window);

	// The GTK infinite loop
	gtk_main();

	return EXIT_SUCCESS;
}

